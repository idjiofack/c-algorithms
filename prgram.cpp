#include <iostream>
using namespace std;
class Array{
	public:		
		void mergeSort(int lowerIndex, int highIndex);
		void display();
		Array(int n);
	private:
		int * container;
		int r;
		int size;
		void merge(int start, int middle, int upperIndex);
		void selectionSort(int start, int end);
};
void Array::display(){

	for(int i=0;i<size;i++){
		cout<< container[i] <<" | ";
	}

}
Array::Array( int n){
	size= n;	
	r=4;
	container = new int[n];
	container[0]=100;
	for(int i =1; i< n; i++){
		container[i]= container[i-1]-5;
	}
}


void Array::merge(int start, int middle, int upperIndex ){	
	int maxInt= 1000;
	int size1=middle-start+2;
	int * pile1 = new int[size1];
	int size2 = upperIndex-middle+1;
	int * pile2 = new int[size2];	
	int pile1browser =0;	
	int pile2browser = 0;	
	for(int i=start; i<= middle ; i++){		
		pile1[pile1browser]=container[i];		
		pile1browser++;		
	}
	pile1[pile1browser]=maxInt; 
	pile1browser=0;
	for(int j=middle+1; j<= upperIndex; j++){		
		pile2[pile2browser]=container[j];
		pile2browser++;
		
	}
	pile2[pile2browser]= maxInt;	
	int k =start;
	pile2browser =0;
	pile1browser = 0;
	while((pile1[pile1browser]!= maxInt)||(pile2[pile2browser]!= maxInt )){
	
		if(pile1[pile1browser]<=pile2[pile2browser]){
			
			if(pile1[pile1browser]!=maxInt){
				container[k]= pile1[pile1browser];
				pile1browser++;
				k++;
			}
			else{
				while(pile2[pile2browser]!=maxInt){
					container[k]= pile2[pile2browser];
					pile2browser++;
					k++;
				}
			}
			
		}
		else{
			if(pile2[pile2browser]!=maxInt){
				container[k]= pile2[pile2browser];
				pile2browser++;
				k++;
			}
			if(pile1[pile1browser]==maxInt){
				while(pile1[pile1browser]!=maxInt){
					container[k]= pile1[pile1browser];
					pile1browser++;
					k++;
				}
			}
		}
	}
	delete(pile1);
	delete(pile2);	
}
void Array::mergeSort(int lowerPos, int higherPos){
	if(higherPos- lowerPos<r){
		selectionSort(lowerPos,higherPos);
	}
	else{
		
		int i = (higherPos- lowerPos)/2;
		mergeSort(lowerPos,lowerPos +i);
		mergeSort(lowerPos+i+1,higherPos);		
		merge(lowerPos,lowerPos+i,higherPos);
	}


}
void Array::selectionSort(int start, int end){
	if(container==NULL)return;
	for(int i=start; i <=end; i++){
		for(int j=i; j<=end; j++){
			if(container[i]>container[j]){
				int temp= container[i];
				container[i]=container[j];
				container[j]=temp;
			}
		}
	}

}
class Node{
	public:
		Node * next;
		Node * previous;
		int data;
		Node( int data);
};
Node::Node(int data){
	next = NULL;
	previous= NULL;
	this->data = data;
}

class List{
	public:
		Node * head;
		Node * deepCopy();
		void addNode( Node * node);
		void display();
		bool isEmpty();
		List(){
			head= NULL;
		}
};
bool List::isEmpty(){
	return (head==NULL);
}
void List::addNode( Node * nodeIn){
	if(nodeIn==NULL) return;
	nodeIn->next=head;
	nodeIn->previous=NULL;
	head = nodeIn;
}
void List::display(){
	Node * browser = head;
	while(browser !=NULL){
		cout<< " "<< browser->data;
		browser=browser->next;
	}
	return;	
}
Node * List::deepCopy(){	
	Node * output= NULL;
	Node * browser = head;
	Node * current = NULL;
	while(browser!=NULL){
		Node * temp = new Node(browser->data);
		temp->next=NULL;
		temp->previous=current;
		if(current!=NULL){
			current->next=temp;
		}
		current = temp;
		if(output==NULL) output = current;
		browser=browser->next;
	}
	return output;
}
class Stack{
	public:
		Stack();
		List * container;
		Node * pop();
		void push(Node * node);

};
Stack::Stack(){
	container = new(List);
	container->head =NULL;
}
Node * Stack::pop(){
	if(container->isEmpty())return NULL;
	Node * ret; Node * temp;
	temp= container->head->next;
	temp->previous=NULL;
	ret = container->head;
	ret->next=NULL;
	container->head=temp;
	return ret;
}
void Stack::push(Node * nodeIn){
	container->addNode(nodeIn);
}
int main(){
	Array a(10);
	a.display();
	cout<< "Sorted table \n";
	a.mergeSort(0,9);
	a.display();


/*
	
	List * a;
	Stack * s;
	s= new(Stack);
	a = new(List) ;
	for(int i=0;i<4; i++){
		a->addNode(new Node(i));
	}
	
	cout<< "voici ma list \n";
	a->display();
	List * b = new(List);
	b->head= a->deepCopy();

	cout<< "\n voici ma list \n";
	b->display();

	for(int i=0;i<4; i++){
		s->push(new Node(i));
	}
	cout<< "this is the first element of my stack"<< s->pop()->data;
	*/
	return 0;
}