#include <iostream>

using namespace std;
bool isPowerOfTwo( int n){
	return (n & (n-1))==0;
}

int main(){
	cout <<"--------------------------------------------------------------------------------"<< endl;
	
	cout << isPowerOfTwo(4)<< endl;
	
	cout <<"--------------------------------------------------------------------------------"<< endl;
	cout << isPowerOfTwo(5)<< endl;
	
	cout <<"--------------------------------------------------------------------------------"<< endl;
	
return 0;
}
