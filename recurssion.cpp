#include <iostream>
#include <vector>
#include <string>
using namespace std;
// I just wanted to write the algorithm by counting since this can be solve with pure math(combinatoric analysis).
//to solve the second part will evolve checking if it is possible to move right or left by checking the array of place where it is impossible to go.
//this is also a version of dfs if we want to make the parallel with graph Theory.
void robotWalk(int n, int right, int down, int & res){
	if(down == n-1 && right == n-1){
		res++;
		return;
	}	
	if(right <n-1){
		robotWalk(n, right+1, down,res);
	} 
	if(down <n-1) {
		robotWalk(n, right, down+1,res);
	}
	return;
}
void print(int * tab, vector<bool> check, int size){
	cout<< "(";
	for(int i=0; i<size; i++){
		if(check[i]) cout<< tab[i] << " ";
	}
	cout<< ") " << endl;
}

void allSubset( int * tab, vector<bool> check, int size, int index){
	if(index == size-1){		
		check[index]= true;
		print(tab,check, size);
		check[index]= false;
		print(tab,check, size);
		return;
	}	
	check[index]= true;	
	allSubset(tab,check,size, index+1);
	check[index]= false;	
	allSubset(tab,check,size, index+1);
	return;	
}
string extract(string s, int position){
	string ch = "";	
	for(int i=0; i< s.size(); i++){
		if(i!=position){			
			ch += s[i];
		}
	}
	return ch;
}

void stringPermutation(string s, int k, int n){
	if(k == n) {
				cout << s<< endl;
	}
	for(int i =k ; i< s.size(); i++){			
		char t = s[k];
		s[k]= s[i];
		s[i] = t;		
		stringPermutation(s, k+1,n);
		 t = s[k];
		s[k]= s[i];
		s[i] = t;		
	}
	return;
}

bool canSpreadHorizon(char ** lang, int i, int j,char color,  int n){
	if((i<0)||(i>=n)||(lang[i][j]== color)) return false;
	return true;
}
bool canSpreadVertical(char ** lang, int i, int j,char color,  int n){
	if((j<0)||(j>=n)||(lang[i][j]== color)) return false;
	return true;
}
bool isValid(int i, int j, int n){
	return ((i>=0)&&(i<n)&& (j>=0)&&(j<n));
}

void paint(char ** land, char color, int i, int j, int n){
	if(!isValid(i,j,n)) return;
	land[i][j]= color;
	if(canSpreadHorizon(land,i+1,j,color,n))paint(land,color,i+1,j,n);
	if(canSpreadHorizon(land,i-1,j,color,n))paint(land,color,i-1,j,n);
	if(canSpreadVertical(land,i,j+1,color,n))paint(land,color,i,j+1,n);
	if(canSpreadVertical(land,i,j+1,color,n))paint(land,color,i,j-1,n);
	return;
}
void parenthesis(int left, int right,char * ch, int count){
	//if(left<0 || right < 1) return;
	if(left == 0 && right == 0){
		cout << ch<< endl;
		for(int i = 0; i< 2*count; i++){
			cout << ch[i];	
		}
		cout <<  endl;;
	} 
	else{
		if(left > 0){
			ch[count]='(';			
			parenthesis(left-1, right, ch, ++count);
		}
		if(right > 0){
			ch[count]=')';
			parenthesis(left, right-1, ch, ++count);
		}
	}	
}
void parenthesisMain( int count){
	char * ch = new char[2*count];
	parenthesis(count, count, ch,0);
	
}

int main(){
	int resHolder = 0;
	int & res = resHolder ;
	int tab[] ={1,2,3,4};
	vector<bool> checkTab = {false, false, false, false};
	robotWalk(4,0,0,res);
	cout<< "The answer is "<< res<< endl;
    cout<< "********************************************************************************";
	allSubset(tab,checkTab,4,0);
	cout<< "********************************************************************************";
	string ch = "1234";
	stringPermutation(ch,0,ch.size());
	cout<< "********************************************************************************";
	parenthesisMain(4);
	return 0;
}