#include <iostream>
#include <string>
#include <stdlib.h> 
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

int maxi(int a, int b){
	if(a<b)return b;
	else return a;
}
int getBiggerGap(vector<int>  v){
	vector<int> a= v;
	int max= 0;
	
	for(int i=0;i < a.size()-1; i++){
		max=a.back()-a[i];
		if(a[i]>=3) return a.back()-a[i];
	}
	
	return max;
}
int solve(vector<int> * v){
	
	vector<int> a= *v;
	/*cout <<"\n";
	
	for(int i=0;i < a.size(); i++){
		cout << a[i]<<"-";
	}
	cout <<"\n";
	
	string ch;
	cin >> ch;*/
	make_heap (a.begin(),a.end());
	int maxVec = a.front();
	sort_heap(a.begin(),a.end());	
	int max =getBiggerGap(a);
	/*cout <<"\n";
	cout<< max;
	cout <<"\n";*/
	if(max<3){
	
		a.pop_back();		
	
			
		if(max=2){
			int interVal =maxVec-1;
			 cout <<"\n";
			 cout << a.back();
			 cout <<"\n";
			if(interVal==a.back()) return maxVec;
			else return 1+maxVec/2;
		}
		else return maxVec;
		
		//return maxi(maxVec,1+a.front());
	}
	else{	
	a.pop_back();
	int middle = maxVec/2;		
	a.push_back(maxVec-middle);
	a.push_back(middle);
	//cout <<"*"<<a.front()<< "*";
		//push_heap (a.begin(),a.end());
	return 1+ solve(&a);
	}
	
		
	
	
}



int solveMinutes(string line){
	
	vector<int> a;
	string ch;
	for(int i=0;i < line.length(); i++){
		if(line[i]!=' ')a.push_back(line[i] - '0');
	}
		
	return solve(&a);
	
}


int main(){
	string line;
  ifstream myfile ("input.in");
   ofstream outputFile ("output.txt");
   if(outputFile.is_open()){
  if (myfile.is_open())
  {
	bool  isFirstLine= true;
	  int step=1;
    while ( getline (myfile,line) )
    {
    
	  if(!isFirstLine){
		getline (myfile,line);		
		outputFile << "Case #";
		outputFile << step;
		outputFile << ": ";	
		outputFile << solveMinutes(line);	
		outputFile << endl;
		step++;
	  }
	    isFirstLine= false;
    }
    myfile.close();
	outputFile.close();
  }
   }

  else cout << "Unable to open file"; 

  return 0;
}
