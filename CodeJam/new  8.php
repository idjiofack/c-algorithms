function FormorderidDocitemid_ICB($var,$vm)
{
  $StrLabel=$vm->StrTable;

  if(!empty($vm->subvm[$var]->Var) and !empty($vm->Var[$var]['SubAttr']['listcols']))
  {
    $listsubvar=explode(",",$vm->Var[$var]['SubAttr']['listcols']);
    $new_docitemid=array();
    $tab_width=array();
    if(!empty($vm->Var[$var]['SubAttr']['sizecols']))
    {
      foreach($vm->Var[$var]['SubAttr']['sizecols'] as $col => $width)
			{
        $tab_width[$col]=$width;
    }
		}

    foreach($listsubvar as $subvalues)
    {
      foreach($vm->subvm[$var]->Var as $id=>$values)
      {
        if($subvalues==$id)
        {
          if(!empty($values['Format']))
          {
            $Formats=explode(',',$values['Format']);
            if ($Formats[1]=='date')
              $input='date';
            elseif ($Formats[1]=='currency')
              $input='currency';
            elseif ($Formats[1]=='qty')
              $input='qty';
            else
              $input='text';
          }
          else
            $input='text';

          if($id=='ITEMQUANTITY')
          {
            $vm->subvm[$var]->Var[$id]['Input']='hidden';
            $vm->subvm[$var]->Var[$id]['View']=3;

            $new_docitemid['QUANTITY']['Type'] = 'char';
            $new_docitemid['QUANTITY']['Input'] = 'text';
            $new_docitemid['QUANTITY']['Format'] = ',';
            $new_docitemid['QUANTITY']['Width']=$tab_width[$id];
            $new_docitemid['QUANTITY']['View'] = 1;
            $new_docitemid['QUANTITY']['AFrom'] = 4;
            $new_docitemid['QUANTITY']['PortValue'] = '';
            $new_docitemid['QUANTITY']['Label'] = $StrLabel['QUANTITY'];
          }
          else if ($id=='CODEUNIT' or $id=='ANAME' or $id=='MANREF' or $id=='MANUFACTURER' or $id=='CONDTYPE' or $id=='CONDITIONNEMENT' or $id=='SHIPDELAY')
          {
            $vm->subvm[$var]->Var[$id]['Input']='hidden';
            $vm->subvm[$var]->Var[$id]['View']=3;
          }
          else
          {
            $vm->subvm[$var]->Var[$id]['Input']=$input;
            $vm->subvm[$var]->Var[$id]['View']=1;
            //$vm->subvm[$var]->Var[$id]['DecPrec']=2;
            if($id=='ADESCRIPTION')
              $vm->subvm[$var]->Var[$id]['Multi']=array('all'=>'linesubtable3','1'=>'linesubtable2');

            if(!empty($StrLabel[$id]))
              $vm->subvm[$var]->Var[$id]['Label'] = $StrLabel[$id];
          }

          if(!empty($tab_width[$id]))
            $vm->subvm[$var]->Var[$id]['Width']=$tab_width[$id];

          $new_docitemid[$id]=$vm->subvm[$var]->Var[$id];

          break;
        }
      }
    }
    $vm->subvm[$var]->Var=$new_docitemid;
    if (!empty($vm->subvm[$var]->Var['CODEUNIT']) && !empty($vm->subvm[$var]->Var['CODEUNIT']['Values']))
      $codeunits=$vm->subvm[$var]->Var['CODEUNIT']['Values'];
    else
      $codeunits=false;
    $newListValues = array();
    foreach($vm->subvm[$var]->ListValues as $Listid=>$Listvalues)
    {
      if(empty($Listvalues['DELETED']) OR ($Listvalues['DELETED']==0 OR ($vm->Values['ISTATUS']<0 AND $Listvalues['DELETED']==2)))
      {
        if (empty($Listvalues['CONDITIONNEMENT']))
          $Listvalues['CONDITIONNEMENT']=1;

        $unit=($codeunits?(!empty($codeunits[$Listvalues['CODEUNIT']])?$codeunits[$Listvalues['CODEUNIT']]:$Listvalues['CODEUNIT']):$Listvalues['CODEUNIT']);
        if($Listvalues['CONDITIONNEMENT']>1)
          $Listvalues['QUANTITY']=round(($Listvalues['ITEMQUANTITY']*$Listvalues['CONDITIONNEMENT']),2).' '.$unit;
        else
          $Listvalues['QUANTITY']=round($Listvalues['ITEMQUANTITY'],2).' '.$unit;

        $description= $Listvalues['ANAME'];
        if(!empty($Listvalues['ADESCRIPTION']))
        {
          if(strlen($description) > 200)
            $description=trim(str_replace("\n","-",$description));
      	    //$description=substr($description,0,50).'...';
          $description.="\n".$Listvalues['ADESCRIPTION'];
        }
        if(!empty($Listvalues['MANREF']))
          $description.="\n".$StrLabel['MANREF'].' '.$Listvalues['MANREF'];
        if($Listvalues['CONDITIONNEMENT']>1)
        	$description.="\n".$StrLabel['CONDITIONNEMENT'].' '.$Listvalues['CONDITIONNEMENT'];
        if(!empty($Listvalues['CONDTYPE']))
          $description.="\n".$Listvalues['CONDTYPE'];
        if(!empty($Listvalues['SHIPDELAY']))
          $description.="\n".sprintf($StrLabel['SHIPDELAY'],$Listvalues['SHIPDELAY']);
        if(defined('SETUP_ARTDURATION') and SETUP_ARTDURATION & 0x3000 and !empty($Listvalues['STARTDATE']) and !empty($Listvalues['ENDDATE']))
          $description.="\n".$StrLabel['STARTDATE'].DisplayDate($Listvalues['STARTDATE'])."-".$StrLabel['ENDDATE'].DisplayDate($Listvalues['ENDDATE']);
        $Listvalues['ADESCRIPTION'] = $description;
        $newListValues[] = $Listvalues;
      }
    }
    // We musn't use "unset($vm->subvm[$var]->ListValues[$Listid])" cause it will break the array index and we'll have problems with price
    // decimal precision when generating the order bill (cause we'll not found the right index to get the currency)
    $vm->SetVarValue($var, $newListValues);
  }
}