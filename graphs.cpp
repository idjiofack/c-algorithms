#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>


using namespace std;

class Graph{
	private:
		int ** graphContainer;
		int vertices;
		int edges;
		bool * marked;
		void addEdge(int nodeDep, int nodeArr);	
		void buildGraph(int numberNode);	
		int getInteger(string a);
	public:		
		Graph(string file);
		void dfs(int node);
};

void Graph::dfs(int node){
	marked[node]= true;	
	cout<< node;
	for(int i=0; i< vertices; i++){
		if(graphContainer[node][i]==1){
			if(!marked[i])dfs(i);
		}
	}
}
int Graph::getInteger(string a){	
	int value = atoi(a.c_str());
	return value;
}
const vector<string> explode(const string& s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

Graph::Graph(string file){
	string line;
 	ifstream myfile (file);
	if (myfile.is_open())
	{
		bool isFirstLine = true;
		while ( getline (myfile,line) )
    		{
			
			vector<std::string> tokens{explode(line, ' ')};
			if(!isFirstLine){     				
				addEdge(getInteger(tokens[0]),getInteger(tokens[1]));
			}
			else {
				buildGraph(getInteger(tokens[0]));
				isFirstLine= false;
			}
   		}
   		myfile.close();
 	 } 

  	else cout << "Unable to open file"; 
	
}


void Graph::buildGraph(int numberNode){	
	graphContainer = new int *[numberNode];
	marked = new bool[numberNode];
	vertices = numberNode;
	for(int i=0; i<numberNode; i++){
		graphContainer[i]= new int[numberNode];
	}
	for(int i=0; i<numberNode; i++){
		marked[i]= false;
	}
	
	for(int i=0; i<numberNode; i++){
		for(int j=0; j<numberNode; j++){			
			graphContainer[i][j]=-1;
		}
	}
	
}
void Graph::addEdge(int nodeDep, int nodeArr){
	graphContainer[nodeDep][nodeArr]= 1;
}

int main(){

Graph * g  = new Graph("input.txt");
g->dfs(0);
return 0;
}