#include <iostream>
#include <string>
using namespace std;

class CompareString{
	public:
		string  getSubstring(string const firstString, string const secondString);
	private:
		void appendChar(string sh, char c);
};

string  CompareString::getSubstring(string const one, string const second){
	int * isLetterFound = new int[52];
	string output;
	
	for(int i=0; i <52; i++){
		isLetterFound[i]=0;	
	}
	for(int j=0; j <one.length(); j++){
		isLetterFound['a'-one[j]]=isLetterFound['a'-one[j]]++;
		
	}
	for(int k=0; k <second.length(); k++){
		if(isLetterFound['a'-second[k]]>=1){
			output = output.append(1,second[k]);
			isLetterFound['a'-second[k]]--;
		}
		
	}
	
	return output;
}

int main(){
	string one = "JeneSaispascequevraiment";
	string two="jenesuivraiment";
	CompareString cp = CompareString();
	cout<< cp.getSubstring(one,two );
	return 0;
}
