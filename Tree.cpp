#include <string>
#include <vector>
#include <iostream>
#include <deque>
#include <map>
#include <math.h>
#include <iomanip>
using namespace std;
class Node{
		
	public:
		Node * leftChild;
		Node * rightChild;
		int value; 
		void print();  
		bool search(int a); 
		Node(int a);  
		void setLeftChild(Node * node);
		void setRightChild(Node * node);			
};
Node::Node(int a){
	value = a;
	this->leftChild = nullptr;
	this->rightChild = nullptr;
}
void Node::print(){
	if(this != nullptr){
		this->leftChild->print();
		cout << this->value;		
		this->rightChild->print();
	}
}
void Node::setLeftChild(Node * node){
	this->leftChild = node;
}
void Node::setRightChild(Node * node){
	this->rightChild= node;
}

Node * arrayToTree(const vector<int> &tab, int start, int end){
	if(end == start){
		return new Node(tab[end]);
	}
	if(end - start==1){
		Node * root = new Node(tab[end]);
		Node * left = new Node(tab[start]);
		root->leftChild = left;
		return root;
	}
	int mid = (end-start)+1;
	int newStart=0;
	int newEnd =0;
	int take =0;
	if((mid % 2)==0){
		take = start + mid/2;		
	} 
	else{
		take = start + (mid-1)/2;
	}
	newStart = min(take+1, end);
	newEnd = max(start, take-1);	
	Node * root = new Node(tab[take]);
	root->leftChild = arrayToTree(tab,start,newEnd);
	cout<< "[ "<< start << " , " << newEnd << " ]"<< endl;  
	root->rightChild= arrayToTree(tab,newStart,end);
	cout<< "[ "<< newStart << " , " << end << " ]"<< endl; 
	return root;
}
map<int , vector<int>>  treeToList( Node * root){
	map<int, vector<int>>  result ;
	if(root== nullptr) return result;
	Node * special = new Node(-1);
	deque<Node *> queue;
	queue.push_back(root);
	queue.push_back(special);
	int counter = 0;
	while(queue.size() > 1){
		vector<int> * list = new vector<int>;
		Node * current = queue.front();
		queue.pop_front();
		while(current->value != -1){
			if(current->leftChild != nullptr )queue.push_back(current->leftChild);
			if(current->rightChild != nullptr ) queue.push_back(current->rightChild);
			list->push_back(current->value);
			current = queue.front();
			queue.pop_front();
		}
		queue.push_back(special);
		result.insert(make_pair(counter++, *list));		
	}
	return result;
}

bool searchValue(Node * root,int a){
	if(root == nullptr) return false ;	
	if( root->value == a){		
		 return true;
	}
	return(searchValue(root->leftChild, a)||searchValue(root->rightChild, a));		
	
}
bool areInSameBranch(bool leftValue, bool rightValue){	
	
	return ((!leftValue && rightValue)||(leftValue && !rightValue));
}
Node * commonAncestor(Node * node, int node1, int node2){	
	if((node == nullptr)||(node->value == node1)||(node->value == node2)) return node;	
	bool leftBranch = searchValue(node->leftChild, node1);		
	bool rightBranch = searchValue(node->rightChild, node2);	
	if(!areInSameBranch(leftBranch,rightBranch)) return node;	
	if(leftBranch == true){
		return  commonAncestor(node->leftChild, node1, node2);		
	} 
	else{
		return commonAncestor(node->rightChild, node1, node2);		 
	}
	
}

int count(Node * root){
	if(root == nullptr)return 0;
	return 1 + count(root->leftChild) + count(root->rightChild);
}

bool isSubTreeWithTheSameRoot(Node * t1, Node * t2){
	if( t2 == nullptr) return true;
	if(t1 == nullptr && t2 != nullptr) return false;
	if(t1->value != t2->value) return false;
	return (isSubTreeWithTheSameRoot(t1->leftChild,t2->leftChild) && isSubTreeWithTheSameRoot(t1->rightChild, t2->rightChild));
	
}
bool isSubTree(Node * t1, Node * t2){
	if(t1 == nullptr || t2== nullptr ) return false;
	if(t1->value == t2->value){
		return isSubTreeWithTheSameRoot(t1,t2);
	}
	return (isSubTree(t1->leftChild, t2)|| isSubTree(t1->rightChild, t2));
}

int sumUpValues(vector< int > cont){
	int res = 0;
	for( auto i: cont){
		res += i;
	}
	return res;
}

void addPathToSol(vector< vector<int>> * res, vector<int> * single){
	res->push_back(*single);
}

void getPaths( Node * root, vector< vector<int>> * res , vector<int > * singlePath, int check ){ 
	if(root== nullptr){
		if(check == 0){
		addPathToSol(res, singlePath);
		return;
		}		
		return;
	}
	singlePath->push_back(root->value);
	getPaths(root->leftChild, res, singlePath,0);
	getPaths(root->rightChild, res, singlePath,1);
	singlePath->pop_back();
	return;
	
}
deque<int> extract(vector<int> * line , int limit){
	deque<int> result;
	int i =0;
	while(i <= limit){
		result.push_back(line->front());
		line->erase(line->begin(), line->begin() +1);
		i++;
	}
	
}
vector< deque<int>>  getPathsSumToN( Node * root, int n){ 
	vector<vector<int>> tempResult;
	vector<deque<int>> result;
	vector<int> singlePath;
	getPaths(root,&tempResult, &singlePath,1);
	for(auto line:tempResult){
		int i= 0;
		while(i<= line.size()){
			int j = 0;
			int tempSum =0;
			while( j<=i){
				tempSum += line[j];
				j++;
			}
			if(tempSum == n){
				deque<int> res = extract(&line, i);
				result.push_back(res);
				i=0;
			}
			if(tempSum >n){
				line.erase(line.begin(), line.begin() +1);
				i--;
			}
			if(tempSum<n){
				i++;
			}
			 
		}
	}
	
return result;	
}


int main(){
	//vector<int> & v{1,2,3,4,5,6,7};	
	int myints[] = {1,2,3,4,5,6,7};
    vector<int> v (myints, myints + sizeof(myints) / sizeof(int) );
	Node * node = arrayToTree(v,0,6);
	node->print();
	cout<< endl;
	cout <<"--------------------------------------------------------------------------------"<< endl;	
	map<int, vector<int>> res = treeToList(node);	
	map<int,vector<int>>::iterator it = res.begin();
	for (it = res.begin(); it != res.end(); ++it){
		 cout << it->first << " => "  ;
		 for(auto v:it->second){
			 cout<< v <<" , ";
		 }
		cout <<  endl; 		
	}	
	cout <<"--------------------------------------------------------------------------------"<< endl;
	Node * resu = commonAncestor(node, 5,7);
	cout<< resu->value<< endl;
	
	cout <<"--------------------------------------------------------------------------------"<< endl;
	
	cout<<"Number of nodes in the tree " << count(node)<< endl;
	cout <<"--------------------------------------------------------------------------------"<< endl;
	vector<vector<int>> temp;
	vector<int> singlePath;
	getPaths(node,&temp , &singlePath,1 );
	int i=1;
	for(auto vec : temp){
		cout<< " "<<i<< "=> ";
		for(auto elt : vec){
			cout<< elt <<",";
		}
		cout<< endl;
		i++;
	}
	
	return 0;
}