#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <map>


using namespace std;

class Graph{
	private:
		map<int, vector<int>> graphContainer;
		int vertices;
		int edges;
		bool * marked;
		void addEdge(int nodeDep, int nodeArr);	
		void buildGraph(int numberNode);	
		int getInteger(string a);
		vector<int> adjacent( int i);
	public:		
		Graph(string file);
		void dfs(int node);
};

void Graph::dfs(int node){
	marked[node]= true;	
	
	for(auto v:adjacent(node)){		
		if(!marked[v])dfs(v);		
	}
	cout<< node;
}
int Graph::getInteger(string a){	
	int value = atoi(a.c_str());
	return value;
}
const vector<string> explode(const string& s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

Graph::Graph(string file){
	string line;
 	ifstream myfile (file);
	if (myfile.is_open())
	{
		bool isFirstLine = true;
		while ( getline (myfile,line) )
    		{
			
			vector<std::string> tokens{explode(line, ' ')};
			if(!isFirstLine){     				
				addEdge(getInteger(tokens[0]),getInteger(tokens[1]));
			}
			else {
				buildGraph(getInteger(tokens[0]));
				isFirstLine= false;
			}
   		}
   		myfile.close();
 	 } 

  	else cout << "Unable to open file"; 
	
}


void Graph::buildGraph(int numberNode){		
	marked = new bool[numberNode];
	vertices = numberNode;	
}
void Graph::addEdge(int nodeDep, int nodeArr){
	if(graphContainer.find(nodeDep)!=graphContainer.end()){
		graphContainer.at(nodeDep).push_back(nodeArr);
	}
	else{
		vector<int> v;
		v.push_back(nodeArr);
		graphContainer.insert(pair<int, vector<int>>(nodeDep,v));
	}
	
}

vector<int> Graph::adjacent( int i){
	if(graphContainer.find(i)!=graphContainer.end()){
		return graphContainer.at(i);
	}
	vector<int> v;
	return v;
}

int main(){
Graph * g  = new Graph("input.txt");
g->dfs(0);
return 0;
}