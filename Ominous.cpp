#include <iostream>
#include <string>
#include <stdlib.h> 
#include <fstream>
using namespace std;

string solveCase(string line){
	int * tab ;
	tab = new int[3];
	int j =0;
	for(int i=0;i < line.length(); i++){
		if(line[i]!=' '){
			tab[j] =  line[i] - '0';
			j++;
		}
	}
	int grid = tab[1]*tab[2];
	int restTofill = grid - tab[0];
	if(tab[0]>grid)  return "GABRIEL";
	if(tab[0]<5){
		
		if(restTofill%tab[0]== 0 && restTofill!=0) return "GABRIEL";
		else return "RICHARD";
	}
	else{
		return "RICHARD";
	}
}
int main(){
	string line;
  ifstream myfile ("input.in");
   ofstream outputFile ("output.txt");
   if(outputFile.is_open()){
  if (myfile.is_open())
  {
	bool  isFirstLine= true;
	  int step=1;
	  
    while ( getline (myfile,line) && step <=64 )
    {
    
	  if(!isFirstLine){
		outputFile << "Case #";
		outputFile << step;
		outputFile << ": ";
		outputFile << solveCase(line);	
		outputFile << endl;
		step++;
	  }
	    isFirstLine= false;
    }
    myfile.close();
	outputFile.close();
  }
   }

  else cout << "Unable to open file"; 

  return 0;
}
